# Promotool Iframe Configuration

## Introduction

HTML5 step allow marketers to design custom experiences to be included in their promotions by including an iframe containing an external HTML5 website.

## Sample Iframe

https://orchextra-samples-pt.s3-eu-west-1.amazonaws.com/index.html?my-param=param

## PromoTool events

This HTML5 application has 3 available events to perform communication between the HTML5 application and the promotional flow:

- **start**: [MANDATORY] This event must be triggered when application is loaded.
- **participate**: [OPTIONAL] This event should be triggered when developer decides to perform participation, it will return information about participation status.
- **end**: [MANDATORY] This event must be trigger to go on with the promotional flow.

The events support multiple parameters:

| Name        | type\*                        | action | participation.comply | participation.choosePrize |
| ----------- | ----------------------------- | ------ | -------------------- | ------------------------- |
| start       | orchextraPromotoolStart       | -      | -                    | -                         |
| participate | orchextraPromotoolParticipate | -      | YES                  | YES                       |
| end         | orchextraPromotoolEnd         | YES    | YES                  | YES                       |

_\* Mandatory_

To send the event to the promotional flow the `window.top.postMessage` method must be used:

```javascript
window.top.postMessage({ type: '[TYPE]' }, '*')
```

Example:

```javascript
window.top.postMessage({ type: 'orchextraPromotoolStart' }, '*')
```

### Actions

The action parameter indicates how promotool will react to the `end event`:

- If you don't send it, promotool will mantain the html5 step, if a button is configured in Promotool Platform, it will be displayed under the HTML5 iframe _(only available for embeded iframe)_.
- **nextStep**: If `nextStep` is sent, the promotion will move to the next step after the `end event` is sent.
- **resetFlow**: If `resetFlow` is sent, the promotion will be reset after the `end event` is sent.

```javascript
window.top.postMessage({ type: 'orchextraPromotoolEnd', action: 'nextStep' }, '*')

window.top.postMessage({ type: 'orchextraPromotoolEnd', action: 'resetFlow' }, '*')

window.top.postMessage({ type: 'orchextraPromotoolEnd', action: '' }, '*')

window.top.postMessage({ type: 'orchextraPromotoolEnd' }, '*')
```

### Comply Parameter

If the HTML5 application is validating the participation into a prize draw, the HTML5 application can indicate wether the user is qualified or not with the `participation.comply` parameter in the `participation event` or `end event`.

```javascript
window.top.postMessage(
  { type: 'orchextraPromotoolParticipate', participation: { comply: true } },
  '*'
)

window.top.postMessage({ type: 'orchextraPromotoolEnd', participation: { comply: true } }, '*')
```

### Choose Prize Parameter

If the HTML5 application is replacing the choose prize step, the prize selected can be sent with the `participation.choosePrize` paramameter from the HTML5 application to the promtion in the `participation event` or `end event`.

```javascript
window.top.postMessage(
  { type: 'orchextraPromotoolParticipate', participation: { choosePrize: 1903 } },
  '*'
)

window.top.postMessage({ type: 'orchextraPromotoolEnd', participation: { choosePrize: 1903 } }, '*')
```

## Perticipation Response Example

TBD

## iFrame Query Params

The HTML5 application will receive through URL the following parameters:

- **promoId:** the promotion Id to get the promotion configuration
- **lang**: language code
- **apiUrl**: the API URL to get configuration, load stylesheet, etc.

> Example: https://mydomain.com/?promoId=194&lang=en&apiUrl=https://pt.orchextra.io

## Get Public Promotion Configuration

In order to set up the HTML5 with the same CSS styles than the promotion, or receive the available list of prizes, etc., Orchextra has a public configuration json accesible via API.

```api
GET <apiUrl>/configuration/<promoId>
```

Example:

```api
GET https://pt.orchextra.io/configuration/194
```

Promotion configuration is a JSON object which includes all promotion public information:

- **prizes**: an array with all promotion's prizes. It includes all information about prizes (title, description, image, expiration date, ...)
- **style**: promotion customization information. Brand color, font.

```json
{
    ...
    "style": {
        "fontFamily": "Bree",
        "brandingColor": "#142E63",
        "secondaryBrandingColor": "#142E63",
        "fontsColor": "#142E63",
        "formBackgroundColor": "#009FD4",
        "borderRadius": "0",
        "buttonBorderBottom": "5"
    }
    ...
}
```

- **templates**: copies, images, etc.
- **others**

> Example configuration: https://pt.orchextra.io/configuration/194 (Santa Spin 2017)
