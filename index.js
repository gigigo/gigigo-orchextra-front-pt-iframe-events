const express = require('express')
const app = express()
const path = require('path')
const statics = path.join(__dirname, '/dist')
app.use('/', express.static(statics))
app.listen(1996, function() {
  /* eslint-disable-next-line */
  console.log('Example app listening on port 1996!')
})
